<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Blank Page</title>

    
    <link href="extra/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="extra/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> 
    <link href="extra/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

     
    <link href="css/sb-admin.css" rel="stylesheet">
     
  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      	<a class="navbar-brand mr-1" href="consignment.html">Pastry</a>

	    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">

	    	<i class="fas fa-bars"></i>

	    </button>

  
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

        <div class="input-group">

          	<input type="text" class="form-control" placeholder="Search Anything" aria-label="Search" aria-describedby="basic-addon2">
           
        </div>

      </form>

  
    

    </nav>

    <div id="wrapper">

  
	    <ul class="sidebar navbar-nav">

	        <li class="nav-item">

		        <a class="nav-link" href="index.html">

		            <i class="fas fa-fw fa-tachometer-alt"></i>
		            <span>Dashboard</span>

		        </a>

	        </li>

	        <li class="nav-item dropdown">

		        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

		            <i class="fas fa-fw fa-folder"></i>
		            <span>Pages</span>

		        </a>
	        </li>

	        <li class="nav-item">

	          	<a class="nav-link" href="charts.html">
	            <i class="fas fa-fw fa-chart-area"></i>
	            <span>Products</span></a>

	        </li>

	        <li class="nav-item">

		        <a class="nav-link" href="">
		            <i class="fas fa-fw fa-table"></i>
		        <span>Users</span>
		    	</a>

	        </li>

	    </ul>

      	<div id="content-wrapper">

        	<div class="container-fluid">

           
          		<ol class="breadcrumb">

            		<li class="breadcrumb-item">

              			<a href="index.html">Dashboard</a>

            		</li>

            		<li class="breadcrumb-item active">Insert Product</li>
          		</ol>

       

            	<div class="card mb-3">

            		<div class="card-header">

              			<i class="fas fa-chart-area"></i>
              			<label>Insert Product</label>

          			</div>

            		<div class="card-body">

            			<form>

			       

			                  		<div class="form-group">
							      <!-- <label for="inputState">State</label> -->
							      <select id="inputState" class="form-control "  >

							      	<option selected>Choose...</option>
							      	<?php include('php/connection.php');

							      		if (!mysqli_connect_errno($con)) {

							      			$code 	=	"SELECT category_id, category_name FROM category";
							      			$query  = mysqli_query($con,$code);
							      			
							      			if ($query) {
							      				
							      				while ($row = $query->fetch_assoc()) {
										        
							        				echo "<option selected >    &nbsp $row[category_id] &nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   $row[category_name] </option>";


										    	}

							      			} else {
							      				header('location:404.html');
							      			}
							      			

							      		} else {
							      			# code...
							      		}
							      		


							      	 ?>
							        
							      </select>
							    </div>

			               
	                  
	                  
	                  		<div class="form-row mb-4" > 

	                    		<div class="col-md-4 mb-3">

	                    			<input type="number" min="0"  class=" form-control  " placeholder="Product quantity" required>

	                       		</div>

			                    <div class="col-md-4 input-group mb-2">

							        <div class="input-group-prepend ">

							            <div class="input-group-text mb-2" >₱</div>

							        </div>

			          				<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Product price" required>

			        			</div>

	                  		</div>

	                   		<div class="form-row">

			                    <div class="col-md-4 mb-4">

			                    	<label for="exampleFormControlTextarea1">Product description</label>
			                     	<input type="date" class=" form-control  " placeholder="Product quantity" required>
			                     
			                    </div>
	                    
	                  		</div>

		                 	<div class="form-group">

							    <label for="exampleFormControlTextarea1">Product description</label>
							    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>

							</div>
	                    
		                    <div class="form-group">

			                    <label for="exampleFormControlFile1">Product image</label>
			                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
		                 
		                 	</div>

	                  		<button type="submit" class="btn btn-primary">Insert</button>
	              			<canvas id="myAreaChart" width="100%" height="30"></canvas> 
	              
	              		</form>

            		</div>

         		</div>

        	</div>
       
	        <footer class="sticky-footer">

	          <div class="container my-auto">
	            <div class="copyright text-center my-auto">
	              <span>Copyright © Basically 2018</span>
	            </div>
	          </div>
	        </footer>

      	</div>
       

    </div>
  
 
    <a class="scroll-to-top rounded" href="#page-top">

      <i class="fas fa-angle-up"></i>

    </a>

    
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      	<div class="modal-dialog" role="document">

        	<div class="modal-content">

          		<div class="modal-header">

            		<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            		<button class="close" type="button" data-dismiss="modal" aria-label="Close">

              			<span aria-hidden="true">×</span>

            		</button>
          		</div>

          		<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>

	          	<div class="modal-footer">

	            	<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
	            	<a class="btn btn-primary" href="login.html">Logout</a>

	          	</div>

        	</div>


      	</div>

    </div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
     
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin.min.js"></script>

 </body>

</html>
