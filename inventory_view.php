<?php include('php/connection.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="dashboard.php">Consignment Database</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inl ine ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        
       
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-folder"></i>
            <span>Employee</span>
          </a>
         
        </li>
        <li class="nav-item">
          <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Consignment</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-chart-area"></i>
            
            <span>Sales</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- Icon Cards-->
         
          
   <div class="form-group mb-5">
     
    <a   href="inventory.php"><button type='submit' name='a' style="width:100px;height:45px; padding-top: 10px; "    class='btn btn-secondary '   method='POST'  > <p >Back</button> </a>
  </div>
 
   <div class="container-fluid ml-5 ">
    <?php 

        $code = "SELECT * FROM product WHERE product_id = $_POST[a]";
        $query = mysqli_query($con , $code);
        if ($query) {

          $row = $query->fetch_assoc();
          $filepath = "img/". $row['product_image'];
          echo "
    <div class='row'>
      <div class='col-md-4 p-3  mb-2  '><img src='php/$row[product_image]' class='img-fluid' alt='Responsive image'></div>
      <div class='col-md-5 p-2'> 
       <div class='form-row mb-3'>
    <div class='col'>
       <label for='formGroupExampleInput'>Product category</label>
      <input type='text' class='form-control' value='$row[product_category]'  readonly>
    </div>
    <div class='col '>
       <label for='formGroupExampleInput'>Product code</label>
      <input type='text' class='form-control  ' value='$row[product_id]' readonly >
    </div>
  </div>
  <div class='form-group'>
    <label for='formGroupExampleInput'>Expiration date</label>
    <input type='date' class='form-control' id='formGroupExampleInput' value='$row[product_expiration_date]' readonly>
  </div>
   
   <label for='formGroupExampleInput'>Product price</label>
  <div class='input-group'>

   <div class='input-group-prepend '>

          <div class='input-group-text'>₱</div>
        </div>
        <input type='text' class='form-control' id='inlineFormInputGroupUsername' value='$row[product_price]'  readonly>
      </div>
    </div>

</div>
    </div>
  <div class='form-group ml-5 col '>
    <label for='formGroupExampleInput '>Product name</label>
    <input type='text' class='form-control col-md-9 ' id='formGroupExampleInput'  value='$row[product_name]' readonly>
  </div>
        <div class='form-group ml-5 col mb-5'>

                   
                 <div class='form-group'>

                  <label for='exampleFormControlTextarea1'>Product description</label>
                  <textarea class='form-control col-md-9' name='product_description' id='exampleFormControlTextarea1' rows='3' readonly>$row[product_description]</textarea>

              </div>

              </div>
    ";



    
           
        } else {
           header('location:404.html');
        }
        

    


     ?>
    

 
 
 
 <!-- Modal -->
        
          

          <!-- Area Chart Example-->
           

          <!-- DataTables Example -->
           <form action="php/fourth.php" method="POST">
       <div class="form-group ml-5 mt-2">
         <button type="button" class="btn btn-secondary  ml-4 col-md-4 " data-toggle="modal" data-target="#editproduct">EDIT</button>
            <?php 
              echo "  <button type='submit' class='btn btn-secondary  ml-5 col-md-4' name='a' value='$row[product_id]'>DELETE  </button>
";


             ?>
            

           
       </div>
        
        </form>
    

      
    

    </div>

   
      <div class="modal fade" id="editproduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
               
      <div class="modal-body">
        <form action="php/Third.php" method="POST">
            <div class="form-group">
                     
                    <select id='inputState' class='form-control' name='product_category'  >

                       
                      <?php 

                        if (!mysqli_connect_errno($con)) {

                          $code   = "SELECT category_id, category_name FROM category";
                          $query  = mysqli_query($con,$code);
                          echo "string";
                          if ($query) {
                            
                            while ($row1 = $query->fetch_assoc()) {
                            
                              echo "<option   value='$row1[category_name]' >      $row1[category_name] </option>";


                          }

                          } else {
                            header('location:404.html');
                          }
                          

                        } else {
                          # code...
                        }
                        


                       ?>
                      
                    </select>
                  </div>
                  <?php 
                  echo "<input class='form-control form-control mb-3' name='product_name' type='text' value='$row[product_name]' required>
                  <input class='form-control form-control mb-3' name='product_id' type='text' value='$row[product_id]' hidden>"


                  ;
                   ?>
          
                                  <div class="form-row"> 


                                    <?php 

                                    echo "
                                          <div class='col-md-6 input-group mb-2'>

                      <div class='input-group-prepend ''>

                          <div class='input-group-text mb-2' >₱</div>

                      </div>

                        <input type='number' min='1' class='form-control' id='inlineFormInputGroup'   name='product_price' value='$row[product_price]' required>

                    </div>



  </div>  


                          <div class='form-row'>

                          <div class='col-md-6 mb-4'>

                            <label for='exampleFormControlTextarea1'>Expiration date</label>
                            <input type='date' class=' form-control' name='product_expiration_date'
                            value='$row[product_expiration_date]' 
                             required>
                              <input type='text' class=' form-control' name='cat'
                            value='$row[product_image]' 
                             required hidden>
                          </div>
                      
                        </div>

                                    " ;

                                     ?>

                        

                          
                      
                      

                        
                         
                    <?php 


                    echo "
                           
                              <div class='form-group'>

                  <label for='exampleFormControlTextarea1'>Product description</label>
                  <textarea class='form-control' name='product_description' id='exampleFormControlTextarea1' rows='3'>$row[product_description]</textarea>

              </div>

                    ";



                     ?>
    
      </div>
      <div class="modal-footer">
       
          

        <button type="submit" class="btn btn-primary">Save changes</button>

     
      </div>
      </form>
    </div>
   
  </div>
</div>
     








        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
         
              <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
           
            
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
