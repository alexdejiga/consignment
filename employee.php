<?php include('php/connection.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="dashboard.php">Consignment Database</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        
       
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="employee.php">
            <i class="fas fa-fw fa-folder"></i>
            <span>Employee</span>
          </a>
         
        </li>
        <li class="nav-item">
          <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Consignment</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-chart-area"></i>
            
            <span>Sales</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Employee</li>
          </ol>

          <!-- Icon Cards-->
          <div class="form-group">
             
            <!-- Button trigger modal -->
  <button type="button" class=" btn-secondary btn mr-3" data-toggle="modal" data-target="#addcategory">
    Add employee
  </button>
 <button type="button" class=" btn-secondary btn mr-3" data-toggle="modal" data-target="#addproduct">
   Add employee
</button>
 

<!-- Modal -->
<div class="modal fade" id="addcategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Adding category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
               <form action="php/Secondary1.php" method="POST">
      <div class="modal-body">

          <input class="form-control form-control" name="category_name" type="text" placeholder="Category name" required> 
    
      </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
   
  </div>
</div>
    

<div class="modal fade" id="addproduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
               
      <div class="modal-body">
        <form action="php/fifth.php" method="POST">
           
       
                                  <div class="form-row mt-2 mb-3  "> 

                          
                          <div class="col-md mb-2">

                            <input type="text " form-control-lgmin="1" name="employee_firstname" class=" form-control-lg form-control col-form-label  " placeholder="First name" required>

                            </div>
                            <div class="col-md mb-2">

                            <input type="text" min="1" name="employee_middlename" class=" form-control form-control-lg col-form-label " placeholder="Middle name" required>

                            </div>
                            <div class="col-md mb-2">

                            <input type="text" min="1" name="employee_lastname" class=" form-control form-control-lg col-form-label " placeholder="Last name" required>

                            </div>
                      
                        </div> 
                         <div class="form-row mb-2"> 

                          
                          <div class="col-md mb-2">

                            <input type="text " form-control-lgmin="1" name="employee_number" class=" form-control-lg form-control col-form-label  " placeholder="Contact number" required>

                            </div>
                            <div class="col-md mb-2">

                            <input type="email" min="1" name="employee_email" class=" form-control form-control-lg col-form-label " placeholder="Email address" required>

                            </div>
                             
                      
                        </div> 
                           <div class="form-group mb-2">
                              <input class="form-control form-control-lg form-control mb-4" name="employee_address" type="text" placeholder="Address" required>


                           </div>
                           <div class="form-row mb-4">
                              <div class="col">
                                <input type="radio" name="employee_gender" value="male" checked> &nbsp Male 
   
                              </div>
                               <div class="col">
                                <input type="radio" name="employee_gender" value="female" checked> &nbsp Female 
   
                              </div>
                               
                               

                           </div>
                        
                          
                         
                             <div class="form-group">
                     
                    <select id='inputState' class='form-control' name='employee_role'  >

                      <option   value='manager' >Manager</option>
                      <option   value='staff' >Staff</option>
                      
                    </select>
                  </div>
    
      </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
   
  </div>
</div>

 
 

<!-- Modal -->
        
          
          <div class="form-group mt-5 table-responsive">
              <table class="table table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">  ID </th> 
      <th scope="col">  Position</th>
      <th scope="col">  Firstname</th>
      <th scope="col">  Middlename</th>
      <th scope="col">  Lastname</th>
      <th scope="col">  Gender</th>
      <th scope="col">  Address</th>
      <th scope="col">  Contact number</th>
      <th scope="col">  Email</th>
      <th scope="col">  Username</th>
      <th scope="col">  Password</th>
      <th scope="col">   </th>
    </tr>
  </thead>
  <tbody>



 
    <?php 

      if (!mysqli_connect_errno($con)) {

          $code= "SELECT * FROM employee";
          $query = mysqli_query($con , $code);
          if ($query) {
              echo "<form action='employee_view.php' method='POST'>";   
             while ($row = $query->fetch_assoc()) {
                       
          
              echo "<tr>";
              echo "<th scope='row'>$row[employee_id]</th>";
               echo "<td>$row[employee_role]</td>";
              echo "<td>$row[employee_firstname]</td>";
              echo "<td>$row[employee_middlename]</td>";
              echo "<td>$row[employee_lastname]</td>";
              echo "<td>$row[employee_gender]</td>";
              echo "<td>$row[employee_address]</td>";
              echo "<td>$row[employee_number]</td>";
              echo "<td>$row[employee_email]</td>";
              echo "<td>$row[employee_username]</td>";
              echo "<td>$row[employee_password]</td>";
             

              echo "<td><button type='submit' name='a' value='$row[employee_id]' class='btn btn-primary' >View</button> </td>";
              echo '</tr>';

 

            }
             echo "<form>";


          } else {
            # code...
          }
          
        
      } else {
        # code...
      }
      

     ?>
 




    
     
  </tbody>
</table>

          </div>

          <!-- Area Chart Example-->
           

          <!-- DataTables Example -->
         

    </div>












        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
