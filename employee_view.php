<?php include('php/connection.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="dashboard.php">Consignment Database</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        
       
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="dashboard.php">
            <i class="fas fa-fw fa-folder"></i>
            <span>Employee</span>
          </a>
         
        </li>
        <li class="nav-item">
          <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Consignment</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-chart-area"></i>
            
            <span>Sales</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- Icon Cards-->
         
          
   <div class="form-group mb-7">
     
    <a   href="employee.php"><button type='submit' name='a' style="width:100px;height:45px; padding-top: 10px; "    class='btn btn-secondary '   method='POST'  > <p >Back</button> </a>
  </div>
 
   <div class="container ml-5    ">
    <?php 

        $code = "SELECT * FROM employee WHERE employee_id = $_POST[a]";
        $query = mysqli_query($con , $code);
        if ($query) {

          $row = $query->fetch_assoc();
          $name = $row['employee_lastname'].", ".$row['employee_firstname']." ".$row['employee_middlename']; 
          echo "
    <div class='row'>
      
    <div class='col-md-9  ml-5 mt-5'> 
       <div class='form-row  mb-2'>
          <div class='col mb-2'>
          <label for='formGroupExampleInput'>Employee ID</label>
          <input type='text' class='form-control col-md-4 ' value='$row[employee_id]'  readonly>
    </div>
    
  </div>


  <div class='form-row '>
          
    

  </div>
  <div class='form-group'>

    <label for='formGroupExampleInput'>Employee name</label>
      <input type='text' class='form-control' id='formGroupExampleInput' value='$name' readonly>
  </div>
  <div class='form-row  mb-2'>
          <div class='col mb-2'>
          <label for='formGroupExampleInput'>Gender</label>
          <input type='text' class='form-control   ' value='$row[employee_gender]'  readonly>
          </div>
              <div class='col mb-2'>
          <label for='formGroupExampleInput'>Position</label>
          <input type='text' class='form-control   ' value='$row[employee_role]'  readonly>
          </div>
          
    
  </div>
  <div class='form-row  mb-2'>
          <div class='col mb-2'>
          <label for='formGroupExampleInput'>Contact number</label>
          <input type='text' class='form-control   ' value='$row[employee_number]'  readonly>
          </div>
              <div class='col mb-2'>
          <label for='formGroupExampleInput'>Email address</label>
          <input type='text' class='form-control   ' value='$row[employee_email]'  readonly>
          </div>
          
    
  </div>
       <div class='form-row  mb-2'>
          <div class='col mb-2'>
          <label for='formGroupExampleInput'>Username</label>
          <input type='text' class='form-control  ' value='$row[employee_username]'  readonly>
          </div>
          <div class='col mb-2'>
          <label for='formGroupExampleInput'>Password</label>
          <input type='text' class='form-control  ' value='$row[employee_password]'  readonly>
          </div>
    
  </div>
 
   
  

 <div class='form-group mb-5'>

    <label for='formGroupExampleInput'>Address</label>
      <input type='text' class='form-control' id='formGroupExampleInput' value='$row[employee_address]' readonly>
  </div>

</div>
    </div>
  
         
    ";



    
           
        } else {
           header('location:404.html');
        }
        

    


     ?>
    

 
 
 
 <!-- Modal -->
        
          

          <!-- Area Chart Example-->
           

          <!-- DataTables Example -->
           <form action="php/fourth.php" method="POST">
       <div class="form-group ml-5 mt-2 mb-5">
         <button type="button" class="btn btn-secondary  ml-4 col-md-4 " data-toggle="modal" data-target="#addproduct">EDIT</button>
            <?php 
              echo "  <button type='submit' class='btn btn-secondary  ml-5 col-md-4' name='a' value='$row[employee_password]'>DELETE  </button>
";


             ?>
            

           
       </div>
        
        </form>
    

      
    

    </div>
<div class="modal fade" id="addproduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
               
      <div class="modal-body">
        <form action="php/seven.php" method="POST">
           <?php 

                echo "
                          <div class='form-row mt-2 mb-3'> 

                          
                          <div class='col-md mb-2'>
                            <label for='formGroupExampleInput'>Firstname</label>
                            <input type='text' form-control-lgmin='1' name='employee_firstname' class='  form-control col-form-label' value='$row[employee_firstname]'  required>

                            </div>
                            <div class='col-md mb-2'>
                              <label for='formGroupExampleInput'>Employee Middlename</label>
                            <input type='text' min='1' name='employee_middlename' class=' form-control  col-form-label' value='$row[employee_middlename]' required>

                            </div>
                            <div class='col-md mb-2'>
                            <label for='formGroupExampleInput'>Lastname</label>
                            <input type='text' min='1' name='employee_lastname' class=' form-control   col-form-label 'value='$row[employee_lastname]' required>

                            </div>
                      
                        </div> 

                         <div class='form-row mb-2'> 

                          
                          <div class='col-md mb-2'>
                            <label for='formGroupExampleInput'>Contact number</label>
                       <input type='text' class='form-control '  name='employee_number' value='$row[employee_number]'    >

                            </div>
                            <div class='col-md mb-2'>
<label for='formGroupExampleInput'>Email address</label>
                          <input type='text' class='form-control ' name='employee_email'  value='$row[employee_email]'  >

                            </div>
                             
                      
                        </div> 
                        <div class='form-row mb-2'> 

                          
                        <div class='col-md mb-2'>
                            <label for='formGroupExampleInput'>Username</label>
                       <input type='text' class='form-control   ' name='employee_username'  value='$row[employee_username]'   >

                            </div>
                            <div class='col-md mb-2'>
<label for='formGroupExampleInput'>Password</label>
                          <input type='text' class='form-control ' name='employee_password'  value='$row[employee_password]'  >

                            </div>
                             
                      
                        </div> 
  <div class='form-group mb-2'>
  <label for='formGroupExampleInput'>Address</label>
                              <input class='form-control   form-control mb-4' name=
                              'employee_address' type='text' value='$row[employee_address]' required>

                               <input class='form-control   form-control mb-4' name=
                              'employee_id' type='text' value='$row[employee_id]'  hidden> 
                           </div>
                             <label for='formGroupExampleInput'>Gender</label>
                           <div class='form-row mb-4'>

                              <div class='col'>
                                <input type='radio' name='employee_gender' value='male' checked> &nbsp Male 
   
                              </div>
                               <div class='col'>
                                <input type='radio' name='employee_gender' value='female' checked> &nbsp Female 
   
                              </div>
                               
                               

                           </div>
                        
                          
                         
                             <div class='form-group'>
                       <label for='formGroupExampleInput'>Position</label>
                    <select id='inputState' class='form-control' name='employee_role'  >

                      <option   value='manager' >Manager</option>
                      <option   value='staff' >Staff</option>
                      
                    </select>
                  </div>

                    " ;





            ?>
       
                              
                        
                         
    
      </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
   
  </div>
</div>

      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
         
              <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
           
            
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
