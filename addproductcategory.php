<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Blank Page</title>

    
    <link href="extra/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="extra/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> 
    <link href="extra/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

     
    <link href="css/sb-admin.css" rel="stylesheet">
     
  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      	<a class="navbar-brand mr-1" href="consignment.html">Pastry</a>

	    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">

	    	<i class="fas fa-bars"></i>

	    </button>

  
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

        <div class="input-group">

          	<input type="text" class="form-control" placeholder="Search Anything" aria-label="Search" aria-describedby="basic-addon2">
           
        </div>

      </form>

  
    

    </nav>

    <div id="wrapper">

  
	    <ul class="sidebar navbar-nav">

	        <li class="nav-item">

		        <a class="nav-link" href="index.html">

		            <i class="fas fa-fw fa-tachometer-alt"></i>
		            <span>Dashboard</span>

		        </a>

	        </li>

	        <li class="nav-item dropdown">

		        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

		            <i class="fas fa-fw fa-folder"></i>
		            <span>Pages</span>

		        </a>

		        <div class="dropdown-menu" aria-labelledby="pagesDropdown">

		            <h6 class="dropdown-header">Login Screens:</h6>
		            <a class="dropdown-item" href="login.html">Login</a>
		            <a class="dropdown-item" href="register.html">Register</a>
		            <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
		            <div class="dropdown-divider"></div>
		            <h6 class="dropdown-header">Other Pages:</h6>
		            <a class="dropdown-item" href="404.html">404 Page</a>
		            <a class="dropdown-item active" href="blank.html">Blank Page</a>

		        </div>

	        </li>

	        <li class="nav-item">

	          	<a class="nav-link" href="charts.html">
	            <i class="fas fa-fw fa-chart-area"></i>
	            <span>Charts</span></a>

	        </li>

	        <li class="nav-item">

		        <a class="nav-link" href="tables.html">
		            <i class="fas fa-fw fa-table"></i>
		        <span>Tables</span>
		    	</a>

	        </li>

	    </ul>

      	<div id="content-wrapper">

        	<div class="container-fluid">

           
          		<ol class="breadcrumb">

            		<li class="breadcrumb-item">

              			<a href="index.html">Dashboard</a>

            		</li>

            		<li class="breadcrumb-item active">New product</li>
          		</ol>

       

            	<div class="card mb-3">

            		<div class="card-header">

              			<i class="fas fa-chart-area"></i>
              			<label>Inserting new product</label>

          			</div>

            		<div class="card-body">

            			<form action="php/Secondary.php" method="POST">

			                <div class="form-row"> 

			                	<div class="col-md-6 mb-3">

	                    			<input type="text"   class=" form-control form-control-lg " placeholder="Category name" name="category_name" required>

	                       		</div>

	                       		<div class="col-md-6 input-group  ">

							        <div class="input-group-prepend mb-3">

							            <div class="input-group-text  " >₱</div>

							        </div>

			          				<input type="number" class="form-control  form-control-lg" id="inlineFormInputGroup" placeholder="Category price" name="category_price" required>

			        			</div>
 
			                </div> 

	                   		<div class="form-row">

			                    
	                  		</div>

		                 	<div class="form-group">

							    <label for="exampleFormControlTextarea1">Product description</label>
							    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="category_description" required></textarea>

							</div>
	                    
		                    <div class="form-group">

			                    <label for="exampleFormControlFile1">Product image</label>
			                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="category_image" required>
		                 
		                 	</div>

	                  		<button type="submit" class="btn btn-primary">Insert</button>
	              			<canvas id="myAreaChart" width="100%" height="30"></canvas> 
	              
	              		</form>

            		</div>

         		</div>

        	</div>
       
	        <footer class="sticky-footer">

	          <div class="container my-auto">
	            <div class="copyright text-center my-auto">
	              <span>Copyright © Basically 2018</span>
	            </div>
	          </div>
	        </footer>

      	</div>
       

    </div>
  
 
    <a class="scroll-to-top rounded" href="#page-top">

      <i class="fas fa-angle-up"></i>

    </a>

    
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      	<div class="modal-dialog" role="document">

        	<div class="modal-content">

          		<div class="modal-header">

            		<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            		<button class="close" type="button" data-dismiss="modal" aria-label="Close">

              			<span aria-hidden="true">×</span>

            		</button>
          		</div>

          		<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>

	          	<div class="modal-footer">

	            	<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
	            	<a class="btn btn-primary" href="login.html">Logout</a>

	          	</div>

        	</div>

      	</div>

    </div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
     
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin.min.js"></script>

 </body>

</html>
