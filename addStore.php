	
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pastry</title>
 
    <link href="extra/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="extra/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
 	<link href="extra/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
 	<link href="extra/css/sb-admin.css" rel="stylesheet">
     
  </head>
  	<body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

     	<a class="navbar-brand mr-1" href="index.html">Bugasan</a>
		<button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">

    		<i class="fas fa-bars"></i>

      	</button>

      
      	<form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

	        <div class="input-group">
	          	
	          	<input type="text" class="form-control" placeholder="Search Anything" aria-label="Search" aria-describedby="basic-addon2">
	           
	        </div>

      	</form>
 

    </nav>

    <div id="wrapper">

      	<ul class="sidebar navbar-nav">

	        <li class="nav-item">

	          	<a class="nav-link" href="index.html">

		            <i class="fas fa-fw fa-tachometer-alt"></i>
		            <span>Dashboard</span>

	          	</a>

	        </li>

	        <li class="nav-item dropdown">

		        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

		            <i class="fas fa-fw fa-folder"></i>
		            <span>Pages</span>

		        </a>

	          	<div class="dropdown-menu" aria-labelledby="pagesDropdown">

		            <h6 class="dropdown-header">Login Screens:</h6>
		            <a class="dropdown-item" href="login.html">Login</a>
		            <a class="dropdown-item" href="register.html">Register</a>

		            <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
		            <div class="dropdown-divider"></div>

		            <h6 class="dropdown-header">Other Pages:</h6>
		            <a class="dropdown-item" href="404.html">404 Page</a>
		            <a class="dropdown-item active" href="blank.html">Blank Page</a>

	         	</div>

	        </li>

	        <li class="nav-item">

		        <a class="nav-link" href="charts.html">

		        	<i class="fas fa-fw fa-chart-area"></i>
		            <span>Charts</span>
                   
		        </a>

	        </li>

	        <li class="nav-item">

	          	<a class="nav-link" href="tables.html">
	            <i class="fas fa-fw fa-table"></i>
	            <span>Tables</span></a>

	        </li>

	    </ul>

      	<div id="content-wrapper"> 

      		<div class="container-fluid">

           
	          	<ol class="breadcrumb">

		            <li class="breadcrumb-item">

		             	<a href="Consignment.html">Dashboard</a>

		            </li>
		            <li class="breadcrumb-item active">Insert Product</li>

	         	</ol>
 

            	<div class="card mb-3">

            		<div class="card-header">

              			<i class="fas fa-chart-area"></i>
              			<label>Add Consignment</label>

          			</div>

           			<div class="card-body">

                  		<form action="php/primary.php" method="POST">

                  			<div class="form-group"> 

                  				<input class="form-control form-control-lg" name="store_name" type="text" placeholder="Store name" required>

               				</div>

							<div class="form-group"> 
                				 
                				 <input class="form-control form-control-lg" name="store_address"type="text" placeholder="Store address" required>

               				</div>  

               				<div class="form-row"> 

			                    <div class="col-md-4 input-group mb-2">
			           
			          				<input type="text" class="form-control col-form-label-lg" name="store_contactperson" id="inlineFormInputGroup" placeholder="Contact person" required>

			        			</div>

                    			<div class="col-md-4 mb-2">

                     				<input type="text" name="store_contactnumber" class=" form-control col-form-label-lg " placeholder="Contact number" required>

                      			</div>
			                    <div class="col-md-4 input-group mb-2">
			           
			          				<input type="email" name="store_email" class="form-control col-form-label-lg" id="inlineFormInputGroup" placeholder="Email address" required>

			        			</div>

                  			</div>

                  			<br>

                  			<div class="form-group">

							    <label for="exampleFormControlTextarea1">Consignment</label>
							    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>

  							</div>

							<div class="form-group">

								<label for="sel1">Select list:</label>
								<select class="form-control" id="sel1" placeholder="a">

									<option value="" selected disabled hidden required>Product </option>
									    <option>1</option>
									    <option>2</option>
									    <option>3</option>
									    <option>4</option>

								</select>

							</div>

							<div class="form-group"> 
                
                        		<h3>Store Description</h3>
                    		</div>

                    		<button type="submit" class="btn btn-primary">Insert</button>


                  		</form>
                  		
                   		<canvas id="myAreaChart" width="100%" height="30"><p>sda</p></canvas>
                  
           			</div>

           		</div>

        	</div>
        
	        <footer class="sticky-footer">

		          <div class="container my-auto">

		            <div class="copyright text-center my-auto">

		              <span>Copyright © Basically 2018</span>

		            </div>

		          </div>
	        </footer>

     	</div>

    </div>
    
    <a class="scroll-to-top rounded" href="#page-top">

      <i class="fas fa-angle-up"></i>

    </a>

 
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	    <div class="modal-dialog" role="document">

	        <div class="modal-content">

		   		<div class="modal-header">

			        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
		            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	    	        
	    	        <span aria-hidden="true">×</span>
			        
			        </button>

		        </div>

		        <div class="modal-body">
		          		Select "Logout" below if you are ready to end your current session.
		        </div>


		        <div class="modal-footer">

		            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
		            <a class="btn btn-primary" href="login.html">Logout</a>

	          	</div>

	        </div>

	    </div>
    </div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

 
    <script src="extra/vendor/jquery/jquery.min.js"></script>
    <script src="extra/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

     
    <script src="extra/vendor/jquery-easing/jquery.easing.min.js"></script>

     
    <script src="extra/js/sb-admin.min.js"></script>

</body>

</html>
