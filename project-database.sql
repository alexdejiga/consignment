-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 30, 2018 at 01:42 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project-database`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10029 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(10000, 'a'),
(10001, 'a'),
(10002, 'a'),
(10003, ''),
(10004, ''),
(10005, ''),
(10006, ''),
(10007, ''),
(10008, 'cat'),
(10009, 'cat'),
(10010, 'cat'),
(10011, 'cat'),
(10012, 'cat'),
(10013, 'a'),
(10014, 'cat'),
(10015, 'cat'),
(10016, 'cats'),
(10017, ''),
(10018, ''),
(10019, 'a'),
(10020, 'A'),
(10021, 'A'),
(10022, 'A'),
(10023, 'A'),
(10024, 'A'),
(10025, 'A'),
(10026, 'caaca'),
(10027, 'caaca'),
(10028, 'caaca');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_lastname` varchar(50) DEFAULT NULL,
  `employee_firstname` varchar(50) DEFAULT NULL,
  `employee_middlename` varchar(50) DEFAULT NULL,
  `employee_username` varchar(50) DEFAULT NULL,
  `employee_password` varchar(50) DEFAULT NULL,
  `employee_role` varchar(50) DEFAULT NULL,
  `employee_address` varchar(50) DEFAULT NULL,
  `employee_number` varchar(50) DEFAULT NULL,
  `employee_email` varchar(50) DEFAULT NULL,
  `employee_gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100012 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_lastname`, `employee_firstname`, `employee_middlename`, `employee_username`, `employee_password`, `employee_role`, `employee_address`, `employee_number`, `employee_email`, `employee_gender`) VALUES
(100000, 'a', 'giveree', '$_POST[employee_middlename]', '$_POST[employee_lastname]', '12345', 'manager', '$_POST[employee_ad33dress]', '$_POST[employee_number]', '$_POST[email]', 'female'),
(100001, 'a', 'alex', 'b', 'dejiga', '12345', 'staff', 'davao city', '09429268161', '', 'female'),
(100002, 'a', 'alex', 'b', 'dejiga', '12345', 'staff', 'davao city', '09429268161', '', 'female'),
(100003, 'a', 'alex', 'b', 'dejiga', '12345', 'staff', 'davao city', '09429268161', 'alexdejiga@gmail.com', 'female'),
(100004, 'a', 'alex', 'b', 'dejiga', '12345', 'staff', 'davao city', '09429268161', 'alexdejiga@gmail.com', 'female'),
(100005, 'a', 'dsadasd', 'adada', 'asdad', '12345', 'staff', 'dadasdasda', '0998', 'sadasdasdasdasd@gmail.com', 'female'),
(100006, 'a', 'dsadasd', 'adada', 'a', '12345', 'manager', 'sdasda', '', '', 'female'),
(100007, 'a', 'dsadasd', 'adada', 'a', '12345', 'manager', 'dadasdasda', '', '', 'female'),
(100008, 'a', 'dsadasd', 'adada', 'a', '12345', 'manager', 'dadasdasda', '', '', 'female'),
(100009, '', 'DEJIGA', 'adada', 'a', '12345', 'manager', 'dadasdasda', '', '', 'female'),
(100010, 'dasda', 'dsadasd', 'adada', 'a', '12345', 'manager', 'dadasdasda', '0998', 'sadasdasdasdasd@gmail.com', 'female'),
(100011, '', 'dsadasd', 'adada', '22', '12345', 'manager', 'dadasdasda', '0998', 'sadasdasdasdasd@gmail.com', 'female');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` varchar(50) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_price` int(20) DEFAULT NULL,
  `product_description` varchar(100) DEFAULT NULL,
  `product_image` varchar(100) DEFAULT NULL,
  `product_expiration_date` date DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_category` (`product_category`)
) ENGINE=MyISAM AUTO_INCREMENT=10028 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_category`, `product_name`, `product_price`, `product_description`, `product_image`, `product_expiration_date`) VALUES
(10027, 'a', 'alex', 34342, 'asdadsa', 'img/77220.png', '2018-11-13'),
(10026, 'a', 'goku', 231, 'sadasdada', 'img/13b66a6f7176ab2680cf612ee10bdda1.jpg', '2018-11-09'),
(10025, 'a', 'baka', 12, 'haha', 'HhWkBQy.jpg', '2018-11-30'),
(10024, 'a', 'baka', 12, 'haha', 'img/download (2).jpg', '2018-11-30'),
(10023, 'a', 'dfsdf432', 43434, '3242', 'img/g.jpg', '2018-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
CREATE TABLE IF NOT EXISTS `store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(50) DEFAULT NULL,
  `store_address` varchar(100) DEFAULT NULL,
  `store_contactperson` varchar(50) DEFAULT NULL,
  `store_email` varchar(50) DEFAULT NULL,
  `store_contactnumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1016 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_address`, `store_contactperson`, `store_email`, `store_contactnumber`) VALUES
(1015, 'kuroki', 'davao city', 'alex@gmail.com', 'alex@gmail.com', '09429268161'),
(1014, 'kuroki', 'davao city', 'alex@gmail.com', 'alex@gmail.com', '09429268161'),
(1013, 'kuroki', 'davao city', 'alex@gmail.com', 'alex@gmail.com', '09429268161');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `course` varchar(50) DEFAULT NULL,
  `DOR` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `name`, `address`, `email`, `course`, `DOR`) VALUES
(3, 'alexdasda', 'davao', 'alexdejiga@gmail.co', 'bsit', '2018-11-26'),
(4, 'alex', 'davao', 'alexdejiga@gmail.co', 'bas', '2018-11-26'),
(8, 'alex', 'dsa', 'dsa@sda', 'das', '2018-11-21');

--
-- Triggers `student`
--
DROP TRIGGER IF EXISTS `add1`;
DELIMITER $$
CREATE TRIGGER `add1` AFTER INSERT ON `student` FOR EACH ROW insert into transac values('DATE()', "ADD")
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `update1`;
DELIMITER $$
CREATE TRIGGER `update1` AFTER UPDATE ON `student` FOR EACH ROW insert into transac values  ('CURRENT_DATE' , 'UPDATE')
$$
DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
